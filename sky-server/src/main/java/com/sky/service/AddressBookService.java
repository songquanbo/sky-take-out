package com.sky.service;

import com.sky.entity.AddressBook;
import com.sky.entity.ShoppingCart;

import java.util.List;

public interface AddressBookService {
    void add(AddressBook addressBook);

    List<AddressBook> list();

    AddressBook selectById(Long id);

    void delete(Long id);

    void update(AddressBook addressBook);

    void updateAddressById(AddressBook addressBook);

    AddressBook getDefault();
}
