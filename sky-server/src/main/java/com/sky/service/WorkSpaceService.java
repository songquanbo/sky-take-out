package com.sky.service;

import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;

public interface WorkSpaceService {
    BusinessDataVO getBusinessData();

    OrderOverViewVO getOverviewOrders();
}
