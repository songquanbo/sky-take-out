package com.sky.service;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;

import java.util.List;

public interface DishService {
    PageResult page(DishPageQueryDTO dishPageQueryDTO);

    void addNewDishWithFlavor(DishDTO dishDTO);

    DishVO  selectDishById(Long id);

    void startOrStop(Integer status, Long id);

    void batchDelete(List<Integer> ids);

    List<Dish> selectByCategoryId(Dish dish);

    void updateDish(DishDTO dishDTO);

    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    List<DishVO> listWithFlavor(Dish dish);


    List<DishItemVO> getBySetmealId(Long id);
}
