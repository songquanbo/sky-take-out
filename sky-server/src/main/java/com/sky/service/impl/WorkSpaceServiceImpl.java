package com.sky.service.impl;

import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.WorkSpaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderOverViewVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class WorkSpaceServiceImpl implements WorkSpaceService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private OrderMapper orderMapper;


    /**
     * 获取今日运营数据
     * @return
     */
    public BusinessDataVO getBusinessData() {
        /*要获取的数据
        * newUsers,orderCompletionRate,turnover,unitPrice,validOrderCount*/

        LocalDate today=LocalDate.now();
        LocalDateTime minTime = LocalDateTime.of(today, LocalTime.MIN);
        LocalDateTime maxTime = LocalDateTime.of(today, LocalTime.MAX);
        Map map = new HashMap();
        map.put("minTime",minTime);
        map.put("maxTime",maxTime);
        map.put("status",null);
        //获取今日新增用户
        Integer newUsers = userMapper.countByMap(map);
        Integer allOrders = orderMapper.countByMap(map);//今日所有订单
        map.put("status",Orders.COMPLETED);
        Integer validOrderCount =orderMapper.countByMap(map);//今日有效订单
        //获取订单完成率
        Double orderCompletionRate=0.0;
        if(allOrders!=0){
            orderCompletionRate=validOrderCount/allOrders.doubleValue();
        }
        //营业额
        Double turnover = orderMapper.sumByMap(map);
        if(turnover==null){
            turnover=0.0;
        }
        //平均客单价
        Double unitPrice=0.0;
        if(validOrderCount!=0){
            unitPrice=turnover/validOrderCount;
        }
        //封装返回值
        return BusinessDataVO.builder()
                .orderCompletionRate(orderCompletionRate)
                .newUsers(newUsers)
                .unitPrice(unitPrice)
                .turnover(turnover)
                .validOrderCount(validOrderCount)
                .build();
    }

    /**
     * 查询订单管理数据
     * @return
     */
    public OrderOverViewVO getOverviewOrders() {

        Map map = new HashMap();
        map.put("minTime",LocalDateTime.of(LocalDate.now(), LocalTime.MIN));
        map.put("maxTime", LocalDateTime.of(LocalDate.now(), LocalTime.MAX));
        map.put("status",null);
        Integer allOrders = orderMapper.countByMap(map);//总订单
        map.put("status",Orders.CANCELLED);
        Integer cancelledOrders = orderMapper.countByMap(map);//已取消
        map.put("status",Orders.COMPLETED);
        Integer completedOrders = orderMapper.countByMap(map);//已完成
        map.put("status",Orders.DELIVERY_IN_PROGRESS);
        Integer deliveredOrders = orderMapper.countByMap(map);//派送中
        map.put("status",Orders.TO_BE_CONFIRMED);
        Integer waitingOrders = orderMapper.countByMap(map);//待接单
        //封装返回值
        return OrderOverViewVO.builder()
                .allOrders(allOrders)
                .cancelledOrders(cancelledOrders)
                .completedOrders(completedOrders)
                .deliveredOrders(deliveredOrders)
                .waitingOrders(waitingOrders)
                .build();
    }

}
