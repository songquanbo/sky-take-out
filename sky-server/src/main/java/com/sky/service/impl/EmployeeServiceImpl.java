package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.AccountLockedException;
import com.sky.exception.AccountNotFoundException;
import com.sky.exception.PasswordErrorException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeMapper employeeMapper;


    /**
     * 员工登录
     *
     * @param employeeLoginDTO
     * @return
     */
    public Employee login(EmployeeLoginDTO employeeLoginDTO) {
        String username = employeeLoginDTO.getUsername();
        String password = employeeLoginDTO.getPassword();
        password=DigestUtils.md5DigestAsHex(password.getBytes());
        //1、根据用户名查询数据库中的数据
        Employee employee = employeeMapper.getByUsername(username);

        //2、处理各种异常情况（用户名不存在、密码不对、账号被锁定）
        if (employee == null) {
            //账号不存在
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //密码比对
        if (!password.equals(employee.getPassword())) {
            //密码错误
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        if (employee.getStatus() == StatusConstant.DISABLE) {
            //账号被锁定
            throw new AccountLockedException(MessageConstant.ACCOUNT_LOCKED);
        }

        //3、返回实体对象
        return employee;
    }

    @Override
    public void save(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        //对象属性拷贝
        BeanUtils.copyProperties(employeeDTO,employee);
        employee.setStatus(StatusConstant.ENABLE);//设置账户状态  0:禁用  1:开启
        //对密码进行md5加密
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        employeeMapper.insert(employee);
    }

    @Override
    public PageResult selectList(EmployeePageQueryDTO employeePageQueryDTO) {
        int page=employeePageQueryDTO.getPage();
        int pagesize=employeePageQueryDTO.getPageSize();
        PageHelper.startPage(page, pagesize);
        Page<Employee> employees =employeeMapper.selectListPage(employeePageQueryDTO);
        PageResult pageResult = new PageResult();
        long total = employees.getTotal();
        List<Employee> result = employees.getResult();
        pageResult.setTotal(total);
        pageResult.setRecords(result);
        return pageResult;
    }

    @Override
    public void updateForStatus(Integer status, Long id) {
        employeeMapper.updateStatusById(status,id);
    }

    @Override
    public Employee selectById(Long id) {
        Employee employee= employeeMapper.selectById(id);
        return employee;
    }

    @Override
    public void update(EmployeeDTO employeeDTO) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO,employee);
        employeeMapper.update(employee);
    }

    @Override
    public void changePassword(PasswordEditDTO passwordEditDTO) {
        //1.获取当前操作者的id
        passwordEditDTO.setEmpId(BaseContext.getCurrentId());
        //2.对传入的旧密码进行md5加密,以便于和数据库中的密码进行比较
        String oldPassword1 = DigestUtils.md5DigestAsHex(passwordEditDTO.getOldPassword().getBytes());
        //3.根据id查询当前员工
        Employee employee = employeeMapper.selectById(passwordEditDTO.getEmpId());
        //4.获取员工密码(加密后的)
        String oldPassword2 = employee.getPassword();
        if(oldPassword1.equals(oldPassword2)){
            //对新密码进行md5加密
            passwordEditDTO.setNewPassword(DigestUtils.md5DigestAsHex(passwordEditDTO.getNewPassword().getBytes()));
            employeeMapper.updatePassword(passwordEditDTO);
            return;
        }
        throw new PasswordErrorException(MessageConstant.PASSWORD_EDIT_FAILED);
    }


}
