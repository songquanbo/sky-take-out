package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.mapper.DishFlavorMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {
      @Autowired
      private DishMapper dishMapper;
      @Autowired
      private DishFlavorMapper dishFlavorMapper;
      @Autowired
    private SetmealMapper setmealMapper;
    @Override
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {
        PageHelper.startPage(dishPageQueryDTO.getPage(),dishPageQueryDTO.getPageSize());
        Page<Dish> page=dishMapper.page(dishPageQueryDTO);
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    @Transactional
    public void addNewDishWithFlavor(DishDTO dishDTO) {
        Dish dish=new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.insert(dish);
        //获取dishId
        Long id = dish.getId();
        List<DishFlavor> list=dishDTO.getFlavors();
        //遍历口味集合,将dishId赋值
        for (DishFlavor dishFlavor : list) {
            dishFlavor.setDishId(id);
        }
        if(list!=null&&list.size()>0){
            dishFlavorMapper.insertBatch(list);
        }
    }

    @Override
    public DishVO  selectDishById(Long id) {
        DishVO dishVO= dishMapper.selectById(id);
        List<DishFlavor> flavorList= dishFlavorMapper.selectByDishId(id);
        dishVO.setFlavors(flavorList);
        return dishVO;
    }

    @Override
    public void startOrStop(Integer status, Long id) {
        dishMapper.startOrStop(status,id);
    }

    @Override
    public void batchDelete(List<Integer> ids) {
        dishMapper.delete(ids);
    }

    @Override
    public List<Dish> selectByCategoryId(Dish dish) {
        List<Dish> dishList= dishMapper.selectByCategoryId(dish);
        
        return dishList;
    }

    @Override
    public void updateDish(DishDTO dishDTO) {
        //修改菜品基本信息
        Dish dish = new Dish();
        BeanUtils.copyProperties(dishDTO,dish);
        dishMapper.updateDish(dish);
        //删除原有的所有口味
        dishFlavorMapper.deleteByDishId(dishDTO.getId());
        //重新插入新的口味
        List<DishFlavor> list = dishDTO.getFlavors();
        for (DishFlavor dishFlavor : list) {
            dishFlavor.setDishId(dishDTO.getId());
        }
        if(list!=null&&list.size()>0){
            dishFlavorMapper.insertBatch(list);
        }
    }
    /**
     * 条件查询菜品和口味
     * @param dish
     * @return
     */
    @Override
    public List<DishVO> listWithFlavor(Dish dish) {
        List<Dish> dishList = dishMapper.selectByCategoryId(dish);

        List<DishVO> dishVOList = new ArrayList<>();

        for (Dish d : dishList) {
            DishVO dishVO = new DishVO();
            BeanUtils.copyProperties(d,dishVO);

            //根据菜品id查询对应的口味
            List<DishFlavor> flavors = dishFlavorMapper.selectByDishId(d.getId());

            dishVO.setFlavors(flavors);
            dishVOList.add(dishVO);
        }

        return dishVOList;
    }

    /**
     * 根据套餐id查询菜品
     * @param id
     * @return
     */
    public List<DishItemVO> getBySetmealId(Long id) {
        return setmealMapper.getDishItemBySetmealId(id);
    }


}
