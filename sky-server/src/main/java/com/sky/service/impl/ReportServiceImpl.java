package com.sky.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.sky.dto.GoodsSalesDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.ReportMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {



      @Autowired
      private OrderMapper orderMapper;
      @Autowired
      private UserMapper userMapper;



    /**
     * 营业额统计
     * @return
     */
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {
        //存放日期
        List<LocalDate> dateList=new ArrayList<>();
        //统计金额
        List<Double> sumAmount=new ArrayList<>();
        while(!begin.equals(end)){
            dateList.add(begin);
            begin=begin.plusDays(1);
        }
        String datestr = StringUtils.join(dateList, ",");
        for (LocalDate localDate : dateList) {
            LocalDateTime minTime = LocalDateTime.of(localDate, LocalTime.MIN);
            LocalDateTime maxTime = LocalDateTime.of(localDate, LocalTime.MAX);
            Map map=new HashMap();
            map.put("minTime",minTime);
            map.put("maxTime",maxTime);
            map.put("status", Orders.COMPLETED);
            Double aDouble = orderMapper.sumByMap(map);
            if(aDouble==null){
                aDouble=0.0;
            }
            sumAmount.add(aDouble);
        }
        String amountstr = StringUtils.join(sumAmount, ",");
        return TurnoverReportVO.builder()
                .dateList(datestr)
                .turnoverList(amountstr)
                .build();

    }

    /**
     * 用户统计
     * @param begin
     * @param end
     * @return
     */
    public UserReportVO getUserStatistics(LocalDate begin, LocalDate end) {
        List<LocalDate> dateList=new ArrayList<>();
        dateList.add(begin);
        while(!begin.equals(end)){
            begin=begin.plusDays(1);
            dateList.add(begin);
        }
        String dateStr = StringUtils.join(dateList, ",");
        //记录当前日期的总用户
        List<Integer> totalList=new ArrayList<>();
        //统计当前日期的新增用户
        List<Integer> increaseUserList=new ArrayList<>();
        for (LocalDate localDate : dateList) {
            LocalDateTime minTime = LocalDateTime.of(localDate, LocalTime.MIN);
            LocalDateTime maxTime = LocalDateTime.of(localDate, LocalTime.MAX);
            Map map=new HashMap();
            map.put("maxTime",maxTime);
            totalList.add(userMapper.countByMap(map));
            map.put("minTime",minTime);
            increaseUserList.add(userMapper.countByMap(map));
        }
        String totalListStr = StringUtils.join(totalList, ",");
        String increaseUserListStr = StringUtils.join(increaseUserList, ",");

        return UserReportVO.builder()
                .newUserList(increaseUserListStr)
                .dateList(dateStr)
                .totalUserList(totalListStr)
                .build();
    }

    /**
     * 订单统计
     * @param begin
     * @param end
     * @return
     */
    public OrderReportVO getOrderStatistics(LocalDate begin, LocalDate end) {
        List<LocalDate> dateList=new ArrayList<>();
        dateList.add(begin);
        while(!begin.equals(end)){
            begin=begin.plusDays(1);
            dateList.add(begin);
        }
        //订单数列表
        List<Integer> orderCountList=new ArrayList<>();
        //有效订单数列表
        List<Integer> validOrderCountList=new ArrayList<>();
        //订单总数
        Integer totalOrderCount=0;
        //有效订单数
        Integer validOrderCount=0;
        for (LocalDate localDate : dateList) {
            LocalDateTime minTime = LocalDateTime.of(localDate, LocalTime.MIN);
            LocalDateTime maxTime = LocalDateTime.of(localDate, LocalTime.MAX);
            //查询每天的订单总数
            Integer dayAllOrders=getOrderCount(minTime,maxTime,null);
            orderCountList.add(dayAllOrders);
            //求和订单总数
            totalOrderCount+=dayAllOrders;
            //查询每天的有效订单总数
            Integer dayValidOrders =getOrderCount(minTime,maxTime,Orders.COMPLETED);
            validOrderCountList.add(dayValidOrders);
            //求和有效订单总数
            validOrderCount+=dayValidOrders;
        }
        //订单完成率
        Double orderCompletionRate=0.0;
        if(totalOrderCount!=0){
            orderCompletionRate=validOrderCount.doubleValue()/totalOrderCount;
        }

        String dateStr = StringUtils.join(dateList, ",");
        String orderCountStr = StringUtils.join(orderCountList, ",");
        String validOrderCountStr = StringUtils.join(validOrderCountList, ",");

        return OrderReportVO.builder()
                .orderCompletionRate(orderCompletionRate)
                .totalOrderCount(totalOrderCount)
                .validOrderCount(validOrderCount)
                .dateList(dateStr)
                .orderCountList(orderCountStr)
                .validOrderCountList(validOrderCountStr)
                .build();
    }

    /**
     * 销量排名统计
     * @param begin
     * @param end
     * @return
     */
    public SalesTop10ReportVO getTop10Statistics(LocalDate begin, LocalDate end) {
        //SQL语句
        LocalDateTime minTime=LocalDateTime.of(begin,LocalTime.MIN);
        LocalDateTime maxTime=LocalDateTime.of(end,LocalTime.MAX);
//        //商品名称列表
//        List<String> productList=new ArrayList<>();
//        //商品数量列表
//        List<Integer> numberList=new ArrayList<>();
       List<GoodsSalesDTO> goodsSalesDTOList=orderMapper.sumNumberOfName(minTime,maxTime);
//        for (GoodsSalesDTO goodsSalesDTO : goodsSalesDTOList) {
//            numberList.add(goodsSalesDTO.getNumber());
//            productList.add(goodsSalesDTO.getName());
//        }
        List<String> numberList = goodsSalesDTOList.stream().map(GoodsSalesDTO::getName).collect(Collectors.toList());


        List<Integer> productList = goodsSalesDTOList.stream().map(GoodsSalesDTO::getNumber).collect(Collectors.toList());

        return SalesTop10ReportVO.builder()
                .numberList(StringUtils.join(productList,","))
                .nameList(StringUtils.join(numberList,","))
                .build();
    }

    /**
     * 导出运营数据报表
     * @param response
     */
    public void exportBusinessData(HttpServletResponse response) {
        //1.查询数据库

        //2.将数据写入文件

        //3.通过输出流下载到客户端浏览器
    }

    /**
     * 根据条件统计订单数量
     * @param minTime
     * @param maxTime
     * @param status
     * @return
     */
    private Integer getOrderCount(LocalDateTime minTime,LocalDateTime maxTime,Integer status){
        Map map=new HashMap();
        map.put("maxTime",maxTime);
        map.put("minTime",minTime);
        map.put("status",status);
        return orderMapper.countByMap(map);
    }
}
