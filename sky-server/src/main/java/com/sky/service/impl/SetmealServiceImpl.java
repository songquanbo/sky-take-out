package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.SetmealDishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    @Override
    public PageResult pageQuerySetmeal(SetmealPageQueryDTO setmealPageQueryDTO) {
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
       Page<SetmealVO> page=setmealMapper.page(setmealPageQueryDTO);
        for (SetmealVO setmealVO : page) {
            setmealVO.setCategoryName(categoryMapper.selectCategoryName(setmealVO.getCategoryId()));
        }
        return new PageResult(page.getTotal(),page.getResult());
    }

    @Override
    public SetmealVO getSetmealById(Long id) {
        //根据id查询到套餐信息
        Setmeal setmeal= setmealMapper.selectById(id);
        SetmealVO setmealVO=new SetmealVO();
        BeanUtils.copyProperties(setmeal,setmealVO);
        //设置分类名
        setmealVO.setCategoryName(categoryMapper.selectCategoryName(setmealVO.getCategoryId()));
        //查询设置菜品
        setmealVO.setSetmealDishes(setmealDishMapper.getDishBySetmealId(setmealVO.getId()));
        return setmealVO;
    }

    @Override
    @Transactional
    public void updateSetmeal(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.update(setmeal);
        //获取套餐id
        List<Long> setmealIds=new ArrayList<>();
        Long id = setmealDTO.getId();
        setmealIds.add(id);
        //删除改套餐下的菜品
        setmealDishMapper.deleteDish(setmealIds);
        //获取新的菜品数据
        List<SetmealDish> setmealDishes=setmealDTO.getSetmealDishes();
        //为每一个新菜品赋值套餐id,为下一步插入做准备
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(id);
        }
        setmealDishMapper.insert(setmealDishes);
    }

    /**
     * 新增套餐
     * @param setmealDTO
     */
    @Transactional
    public void save(SetmealDTO setmealDTO) {
        Setmeal setmeal = new Setmeal();
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        BeanUtils.copyProperties(setmealDTO,setmeal);
        setmealMapper.insert(setmeal);
        for (SetmealDish setmealDish : setmealDishes) {
            setmealDish.setSetmealId(setmeal.getId());
        }
        setmealDishMapper.insert(setmealDishes);
    }

    /**
     * 根据id删除套餐
     * @param ids
     */
    public void delete(List<Long> ids) {
        setmealMapper.delete(ids);
        setmealDishMapper.deleteDish(ids);
    }

    /**
     * 启售禁售套餐
     * @param status
     * @param id
     */
    public void startOrStop(Integer status, Long id) {
        setmealMapper.startOrStop(status,id);
    }

    /*******************************************************************   user
     * user根据分类id查询套餐
     * @param categoryId
     * @return
     */
    public List<Setmeal> getByCategoryId(Long categoryId) {
        List<Setmeal> setmealList=setmealMapper.getByCategoryId(categoryId);
        return setmealList;
    }
}
