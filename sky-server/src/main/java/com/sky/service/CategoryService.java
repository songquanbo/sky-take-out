package com.sky.service;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;

import java.util.List;

public interface CategoryService {
    void deleteById(Long id);

    void insert(CategoryDTO categoryDTO);

    PageResult sleectPageQuery(CategoryPageQueryDTO categoryService);

    void update(CategoryDTO categoryDTO);

     List<Category> selectByType(Integer type);

    void changeStatus(Integer status, Long id);
}
