package com.sky.controller.user;

import com.sky.entity.AddressBook;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.AddressBookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user/addressBook")
@Api(tags = "地址簿相关接口")
@Slf4j
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    @PostMapping
    @ApiOperation("添加地址簿")
    public Result add(@RequestBody AddressBook addressBook){
        log.info("添加地址簿:{}",addressBook);
        addressBookService.add(addressBook);
        return  Result.success();
    }

    @GetMapping("/list")
    @ApiOperation("根据当前用查询地址簿")
    public Result<List<AddressBook>> list(){
        log.info("查询所有地址簿");
        List<AddressBook> addressBooks=  addressBookService.list();
        return Result.success(addressBooks);
    }
    @GetMapping("/{id}")
    @ApiOperation("根据id查询地址")
    public Result<AddressBook> selectById(@PathVariable Long id){
        log.info("根据id:{}查询地址",id);
        AddressBook addressBook=  addressBookService.selectById(id);
        return Result.success(addressBook);
    }
   @DeleteMapping
   @ApiOperation("根据id删除地址")
   public Result deleteById(Long id){
     log.info("根据id:{}删除地址",id);
       addressBookService.delete(id);
     return Result.success();
   }

    @PutMapping
    @ApiOperation("根据id修改地址")
    public Result updateById(@RequestBody AddressBook addressBook){
        log.info("修改地址");
        addressBookService.update(addressBook);
        return Result.success();
    }

    @PutMapping("/default")
    @ApiOperation("设置为默认地址")
    public Result updateAddressById(@RequestBody AddressBook addressBook){
        log.info("设置为默认地址");
        addressBookService.updateAddressById(addressBook);
        return Result.success();
    }

    @GetMapping("/default")
    @ApiOperation("查询默认地址")
    public Result<AddressBook> getDefault(){
        log.info("查询默认地址");
        AddressBook addressBook= addressBookService.getDefault();
        return Result.success(addressBook);
    }
}
