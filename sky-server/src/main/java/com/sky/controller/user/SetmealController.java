package com.sky.controller.user;


import com.sky.entity.Setmeal;

import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.service.SetmealService;
import com.sky.vo.DishItemVO;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("userSetmealController")
@RequestMapping("user/setmeal")
@Api(tags = "C端套餐相关接口")
public class SetmealController {


     @Autowired
     private SetmealService setmealService;

     @Autowired
     private DishService dishService;



    @GetMapping("/list")
    @ApiOperation("根据categoryId查询套餐")
    @Cacheable(cacheNames = "setmealCache",key="#categoryId")
    public Result<List<Setmeal>> getByCategoryId(Long categoryId){
        List<Setmeal> setmealList= setmealService.getByCategoryId(categoryId);
        return Result.success(setmealList);
    }
    @GetMapping("/dish/{id}")
    public Result<List<DishItemVO>> getDishById(@PathVariable Long id){
        List<DishItemVO> dishItemVOS= dishService.getBySetmealId(id);
        return Result.success(dishItemVOS);
    }

}
