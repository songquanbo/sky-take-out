package com.sky.controller.admin;

import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrderService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("adminOrderController")
@RequestMapping("admin/order")
@Slf4j
@Api(tags = "订单管理相关接口")
public class OrderController {


     @Autowired
     private OrderService orderService;

    @GetMapping("/conditionSearch")
    public Result<PageResult> pageConditionSearch(OrdersPageQueryDTO ordersPageQueryDTO){
        log.info("根据条件分页查询订单");
           return Result.success(orderService.page(ordersPageQueryDTO));
    }
}
