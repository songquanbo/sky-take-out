package com.sky.controller.admin;

import com.sky.constant.MessageConstant;
import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/admin/common")
@Slf4j
public class CommonController {

    @Autowired
    public AliOssUtil aliOssUtil;






    @PostMapping("/upload")
    @ApiOperation("文件上传")
    public Result<String> upload(MultipartFile file)  {//注意参数名要与前端保持一致
        String originalName=file.getOriginalFilename();
        String extension = originalName.substring(originalName.lastIndexOf("."));
        String newName= UUID.randomUUID()+extension;
        String uploadURL = null;
        try {
            uploadURL = aliOssUtil.upload(file.getBytes(),newName);
            log.info("文件上传:{}",uploadURL);
            return Result.success(uploadURL);
        } catch (IOException e) {
            log.error("文件上传失败");
            e.printStackTrace();
        }
        return Result.error(MessageConstant.UPLOAD_FAILED);
    }
}
