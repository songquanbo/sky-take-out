package com.sky.controller.admin;


import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("adminDishController")
@RequestMapping("/admin/dish")
@Slf4j
@Api(tags = "菜品管理")
public class DishController {
    @Autowired
    private DishService dishService;

    @GetMapping("/page")
    @ApiOperation("菜品分页查询")
    public Result<PageResult> dishPageQuery(DishPageQueryDTO dishPageQueryDTO){
        log.info("菜品分页查询{}",dishPageQueryDTO);
        PageResult pageResult= dishService.page(dishPageQueryDTO);
       return Result.success(pageResult);
    }
    @PostMapping
    @ApiOperation("新增菜品")
    @CacheEvict(cacheNames = "dishCache",allEntries = true)
    public Result addNewDish(@RequestBody DishDTO dishDTO){
      log.info("新增菜品{}",dishDTO);
        dishService.addNewDishWithFlavor(dishDTO);
      return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("根据菜品id查询菜品")
    public Result<DishVO> selectDishById(@PathVariable Long id){
      log.info("根据id查询菜品{}",id);
        DishVO dishVO= dishService.selectDishById(id);
        return Result.success(dishVO);
    }
    @PostMapping("/status/{status}")
    @ApiOperation("启售禁售菜品")
    @CacheEvict(cacheNames = "dishCache",allEntries = true)
    public Result startOrStop(@PathVariable Integer status,Long id){
        log.info("起售与禁售菜品id:{},status:{}",id,status);
        dishService.startOrStop(status,id);
        return Result.success();
    }
    @DeleteMapping
    @ApiOperation("批量删除菜品")
    @CacheEvict(cacheNames = "dishCache",allEntries = true)
    public Result batchDelete(@RequestParam("ids") List<Integer> ids){
        log.info("批量删除ids={}",ids);
        dishService.batchDelete(ids);
        return Result.success();
    }
    @GetMapping("/list")
    @ApiOperation("根据分类id查询菜品")
    public Result<List<Dish>> selectByCategoryId(Long categoryId){
        Dish dish = new Dish();
        dish.setCategoryId(categoryId);
        List<Dish> dishList= dishService.selectByCategoryId(dish);
       return  Result.success(dishList);
    }
    @PutMapping
    @ApiOperation("修改菜品")
    @CacheEvict(cacheNames = "dishCache",allEntries = true)
    public Result updateDish(@RequestBody DishDTO dishDTO){
        dishService.updateDish(dishDTO);
        return Result.success();
    }
}
