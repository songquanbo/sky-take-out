package com.sky.controller.admin;


import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController("adminCategoryController")
@RequestMapping("/admin/category")
@Slf4j
@Api(tags = "分类管理")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @DeleteMapping
    @ApiOperation("根据id删除分类")
    public Result deleteById(Long id){
        log.info("删除分类{}",id);
        categoryService.deleteById(id);
        return Result.success();
    }
    @PostMapping
    @ApiOperation("添加分类")
    public Result addCategory(@RequestBody CategoryDTO categoryDTO){
        log.info("添加分类{}",categoryDTO);
        categoryService.insert(categoryDTO);
        return Result.success();
    }
    @GetMapping("/page")
    @ApiOperation("分类分页查询")
    public Result<PageResult> sleectPageQuery(CategoryPageQueryDTO categoryPageQueryDTO){
        log.info("分类分页查询{}",categoryPageQueryDTO);
       PageResult pageResult= categoryService.sleectPageQuery(categoryPageQueryDTO);
       return Result.success(pageResult);
    }
    @PutMapping
    @ApiOperation("修改分类")
    public Result update(@RequestBody CategoryDTO categoryDTO){
      log.info("修改分类{}",categoryDTO);
        categoryService.update(categoryDTO);
        return Result.success();
    }
    @GetMapping("/list")
    @ApiOperation("根据类型查询分类")
    public Result<List<Category>> selectByType(Integer type){
        log.info("根据类型查询分类{}",type);
        List<Category> list=categoryService.selectByType(type);
        return Result.success(list);
    }
    @PostMapping("status/{status}")
    public Result startOrStopType(@PathVariable Integer status,Long id){
        categoryService.changeStatus(status,id);
        return Result.success();
    }

}
