package com.sky.controller.admin;

import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;

import com.sky.entity.Setmeal;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.SetmealService;
import com.sky.vo.SetmealVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import java.sql.ResultSet;
import java.util.List;

@RestController
@RequestMapping("/admin/setmeal")
@Slf4j
@Api(tags = "套餐管理")
public class SetmealController {

     @Autowired
     private SetmealService setmealService;


     @GetMapping("/page")
     @ApiOperation("分页查询套餐")
    public Result<PageResult> pageQuerySetmeal( SetmealPageQueryDTO setmealPageQueryDTO){
         log.info("分页查询套餐");
       PageResult pageResult= setmealService.pageQuerySetmeal(setmealPageQueryDTO);
       return Result.success(pageResult);
    }
    @PostMapping
    @ApiOperation("新增套餐")
    @CacheEvict(cacheNames = "setmealCache",key="#setmealDTO.categoryId")
   public Result save(@RequestBody SetmealDTO setmealDTO){
         log.info("新增套餐:{}",setmealDTO);
        setmealService.save(setmealDTO);
         return Result.success();
   }
   @GetMapping("/{id}")
   @ApiOperation("根据id查询套餐")
   public Result<SetmealVO> getSetmealById(@PathVariable Long id){
       log.info("根据id:{}查询套餐",id);
       SetmealVO setmealVO= setmealService.getSetmealById(id);
      return Result.success(setmealVO);
   }
    @PutMapping
    @ApiOperation("修改套餐")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
   public Result updateSetmeal(@RequestBody SetmealDTO setmealDTO){
        log.info("修改套餐:{}",setmealDTO);
        setmealService.updateSetmeal(setmealDTO);
       return Result.success();
   }
   @DeleteMapping
   @ApiOperation("根据id批量删除套餐")
   @CacheEvict(cacheNames = "setmealCache",allEntries = true)
   public Result deleteSetmeal(@RequestParam("ids") List<Long> ids){
         log.info("根据id批量删除套餐");
       setmealService.delete(ids);
         return Result.success();
   }
    @PostMapping("/status/{status}")
    @ApiOperation("启用禁用套餐")
    @CacheEvict(cacheNames = "setmealCache",allEntries = true)
   public Result startOrStop(@PathVariable Integer status,Long id){
        log.info("修改套餐id为:{}的状态:{}",id,status);
        setmealService.startOrStop(status,id);
         return  Result.success();
   }


}
