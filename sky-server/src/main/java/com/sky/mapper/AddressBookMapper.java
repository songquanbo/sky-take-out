package com.sky.mapper;

import com.sky.entity.AddressBook;
import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AddressBookMapper {
    @Insert("insert into sky_take_out.address_book(user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label)" +
            " VALUES(#{userId}, #{consignee}, #{sex},#{phone},#{provinceCode}, #{provinceName}, #{cityCode}, #{cityName}, #{districtCode}, #{districtName}, #{detail}, #{label}) ")
    void insert(AddressBook addressBook);

    /**
     * 根据userId查询所有地址
     * @param addressBook
     * @return
     */

    List<AddressBook> selectList(AddressBook addressBook);

    /**
     * 根据id查询地址
     * @param addressBook
     * @return
     */
    @Select("select * from sky_take_out.address_book where id=#{id}")
    AddressBook selectById(AddressBook addressBook);

    /**
     * 根据id删除地址
     * @param id
     */
    @Delete("delete from sky_take_out.address_book where id=#{id};")
    void delete(Long id);

    /**
     * 根据id修改地址
     * @param addressBook
     */
    void update(AddressBook addressBook);

    /**
     * 设置默认地址
     * @param addressBook
     */
    @Update("update sky_take_out.address_book set is_default=#{isDefault} where id=#{id}")
    void updateAddressById(AddressBook addressBook);

    /**
     * 将所有的isDefault置为为0
     * @param addressBook
     */
    @Update("update sky_take_out.address_book set is_default=#{isDefault} where user_id=#{userId}")
    void setDefault(AddressBook addressBook);

    /**
     * 查询默认地址
     * @return
     */
    @Select("select * from sky_take_out.address_book where is_default='1'")
    AddressBook selectDefault();
}
