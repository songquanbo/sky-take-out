package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.*;

@Mapper
public interface EmployeeMapper {

    /**
     * 根据用户名查询员工
     * @param username
     * @return
     */
    @Select("select * from sky_take_out.employee where username = #{username}")
    Employee getByUsername(String username);


    //添加新员工
    @Insert("insert into sky_take_out.employee( name, username, password, phone, sex, id_number, status, create_time, update_time, create_user, update_user) " +
            "values(#{name},#{username},#{password},#{phone},#{sex},#{idNumber},#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    @AutoFill(OperationType.INSERT)
    void insert(Employee employee);

    //分页查询员工
    Page<Employee> selectListPage(EmployeePageQueryDTO employeePageQueryDTO);


     @Update("update sky_take_out.employee set status=#{status} where id=#{id}")
    void updateStatusById(Integer status, Long id);

     //根据id查询员工
    @Select("select * from sky_take_out.employee where id=#{id}")
    Employee selectById(Long id);

    //更新员工信息
    @AutoFill(OperationType.UPDATE)
    @Update("update sky_take_out.employee set name=#{name}, username=#{username}, " +
            "phone=#{phone}, sex=#{sex}, id_number=#{idNumber}, update_time=#{updateTime}, update_user=#{updateUser} where id=#{id}" )
    void update(Employee employee);

    //修改密码
    @Update("update sky_take_out.employee set password=#{newPassword} where id=#{empId}")
    void updatePassword(PasswordEditDTO passwordEditDTO);
}
