package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.dto.GoodsSalesDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper {

    void  insert(Orders orders);
    @Select("select * from sky_take_out.orders where number=#{outTradeNo};")
    Orders getByNumber(String outTradeNo);

    void update(Orders orders);

    /**
     * 查询超时订单
     * @param status
     * @param time
     * @return
     */
    @Select("select * from sky_take_out.orders where status=#{status} and order_time<#{time}")
    List<Orders> getByStstusAndOrderTime(Integer status, LocalDateTime time);

    /**
     * 根据id查询订单
     * @param id
     * @return
     */
    @Select("select * from sky_take_out.orders where id=#{id}")
    Orders getById(Long id);

    /**
     * 统计营业额
     * @param map
     * @return
     */
    @Select("select sum(amount) from sky_take_out.orders where status=#{status} and order_time>=#{minTime} and order_time<=#{maxTime}")
    Double sumByMap(Map map);

    /**
     * 根据条件动态查询订单数或有效数
     * @param map
     * @return
     */
    Integer countByMap(Map map);

    Page<Orders> page(OrdersPageQueryDTO ordersPageQueryDTO);

    /**
     * 销量排名查询
     * @param begin
     * @param end
     * @return
     */
    @Select("SELECT od.name name,SUM(od.number) number  FROM sky_take_out.order_detail od,sky_take_out.orders o\n" +
            "        WHERE od.`order_id`=o.`id` AND o.`status`=5 AND o.order_time>#{begin}\n" +
            "         AND o.order_time<#{end} GROUP BY od.name ORDER BY number DESC LIMIT 0,10")
    List<GoodsSalesDTO> sumNumberOfName(LocalDateTime begin, LocalDateTime end);
}
