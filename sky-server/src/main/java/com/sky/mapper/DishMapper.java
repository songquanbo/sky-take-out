package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface DishMapper {

    Page<Dish> page(DishPageQueryDTO dishPageQueryDTO);

    @Select("select count(*) from sky_take_out.dish where category_id=#{categoryId}")
    Integer countByCategoryId(Long id);

    @AutoFill(OperationType.INSERT)
    void insert(Dish dish);

   @Select("select * from sky_take_out.dish where id=#{id}")
   DishVO selectById(Long id);


    @Update("update sky_take_out.dish set status=#{status} where id=#{id}")
    void startOrStop(Integer status, Long id);

    void delete(List<Integer> ids);


   @Select("select * from sky_take_out.dish where category_id=#{categoryId}")
    List<Dish> selectByCategoryId(Dish dish);


    @AutoFill(OperationType.UPDATE)
    void updateDish(Dish dish);


}
