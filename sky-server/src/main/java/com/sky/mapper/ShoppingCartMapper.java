package com.sky.mapper;

import com.sky.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface ShoppingCartMapper {
    /**
     * 查询购物车
     * @param shoppingCart
     * @return
     */
    List<ShoppingCart> list(ShoppingCart shoppingCart);

    /**
     * 更新购物车商品数量
     * @param cart
     */
    @Update("update sky_take_out.shopping_cart set number=#{number}  where id=#{id}")
    void updateNumberById(ShoppingCart cart);

    /**
     * 添加购物车数据
     * @param shoppingCart
     */
    void insert(ShoppingCart shoppingCart);

    /**
     * 根据用户id查询购车
     * @param userId
     */

    List<ShoppingCart> selectListByuerId(Long userId);

    /**
     * 删除一件商品
     * @param shoppingCart
     */

    void delete(ShoppingCart shoppingCart);
}
