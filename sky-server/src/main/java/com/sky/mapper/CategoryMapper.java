package com.sky.mapper;

import com.github.pagehelper.Page;
import com.sky.annotation.AutoFill;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CategoryMapper {

    @Delete("delete from sky_take_out.category where id=#{id}")
    //根据id删除分类
    void deleteById(Long id);


        //添加分类
    void insert(Category category);


    //分页查询
    Page<Category> selectPageQuery(CategoryPageQueryDTO categoryPageQueryDTO);


    //修改分类
    @Update("update sky_take_out.category set name=#{name},sort=#{sort} where id=#{id}")
    @AutoFill(OperationType.UPDATE)
    void update(Category category);

    //根据分类类型查询

    List<Category> selectByType(Integer type);

    //根据id启用禁用用户
    @Update("update sky_take_out.category set status=#{status} where id=#{id}")
    void changeStatus(Integer status, Long id);
    @Select("select name from sky_take_out.category where id=#{categoryId}")
    String selectCategoryName(Long id);
    @Select("select type from sky_take_out.category where id=#{id}")
    Integer selectTypeById(Long id);
    @Select("select count(*) from sky_take_out.category")
    int countCategory(Integer type);
}
