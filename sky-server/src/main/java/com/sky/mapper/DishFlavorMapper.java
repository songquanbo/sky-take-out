package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface DishFlavorMapper {

    //批量插入口味数据
    void insertBatch(List<DishFlavor> list);




    List<DishFlavor> selectByDishId(Long id);

    void updateDishFlavor(List<DishFlavor> flavors);
     @Delete("delete from sky_take_out.dish_flavor where dish_id=#{id};")
    void deleteByDishId(Long id);
}
