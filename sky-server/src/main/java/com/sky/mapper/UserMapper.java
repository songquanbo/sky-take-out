package com.sky.mapper;

import com.sky.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.Map;

@Mapper
public interface UserMapper {
    @Select("select * from sky_take_out.user where openid=#{openid}")
    User getByOpenid(String openid);

    void insert(User user);
    @Select("select * from sky_take_out.user where id=#{userId};")
    User getById(Long userId);


    /**
     * 根据条件查询总的用户以及新增用户
     * @param map
     * @return
     */
    Integer countByMap(Map map);
}
