package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.SetmealDTO;
import com.sky.entity.SetmealDish;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealDishMapper {
    @Select("select * from sky_take_out.setmeal_dish where setmeal_id=#{setmealId}")
    public List<SetmealDish> getDishBySetmealId(Long setmealId);


    /**
     * 批量删除套餐下的菜品
     * @param setmealIds
     */
    void deleteDish(List<Long> setmealIds);

    /**
     * 插入菜品
     * @param setmealDishes
     */
    void insert(List<SetmealDish> setmealDishes);
}
