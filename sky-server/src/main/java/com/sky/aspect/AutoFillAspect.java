package com.sky.aspect;

import com.sky.annotation.AutoFill;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Aspect
@Component
@Slf4j
public class AutoFillAspect {
   //切入点,拦截所有mapper包下的所有的类的方法并且带有AutoFill注解的方法
    @Pointcut("execution(* com.sky.mapper.*.*(..))&&@annotation(com.sky.annotation.AutoFill)")
    public void autoFillPointCut(){}

    @Before("autoFillPointCut()")//设置通知类型
    public void autoFill(JoinPoint joinPoint)  {
         log.info("开始进行公共字段的填充...");
        MethodSignature signature = (MethodSignature)joinPoint.getSignature();//获取签名方法
        Method method = signature.getMethod();//获取方法对象
        AutoFill autofill = method.getAnnotation(AutoFill.class);//获取注解对象
        OperationType operationType = autofill.value();//获取注解的value值
        Object[] args = joinPoint.getArgs();//获取方法中的参数
        if(args==null||args.length==0){
            return;
        }
        Object entity = args[0];//约定第一个参数为要自动填充属性值的实体类
        LocalDateTime now = LocalDateTime.now();
        Long currentId = BaseContext.getCurrentId();
        try {
            //判断方法是update还是insert
            if(operationType==OperationType.INSERT){
                    Method setCreateTime = entity.getClass().getDeclaredMethod("setCreateTime", LocalDateTime.class);
                    Method setUpdateTime = entity.getClass().getDeclaredMethod("setUpdateTime", LocalDateTime.class);
                    Method setUpdateUser = entity.getClass().getDeclaredMethod("setCreateUser", Long.class);
                    Method setCreateUser = entity.getClass().getDeclaredMethod("setUpdateUser", Long.class);
                    setCreateTime.invoke(entity,now);
                    setUpdateTime.invoke(entity,now);
                    setUpdateUser.invoke(entity,currentId);
                    setCreateUser.invoke(entity,currentId);

            }else if(operationType==OperationType.UPDATE){
                Method setUpdateTime = entity.getClass().getDeclaredMethod("setUpdateTime", LocalDateTime.class);
                Method setUpdateUser = entity.getClass().getDeclaredMethod("setUpdateUser", Long.class);
                setUpdateTime.invoke(entity,now);
                setUpdateUser.invoke(entity,currentId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
